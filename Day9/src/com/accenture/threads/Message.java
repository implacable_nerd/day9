package com.accenture.threads;

public class Message extends Thread{
	public void run() {
		try {
			System.out.println("Curent \nthread id: "+getId()+"\nthread name: "+getName()+"\nthread priority: "+getPriority()+" successfully running");	
		}
		catch(Exception e) {
			System.out.println("Exception caught");
		}
	}
}