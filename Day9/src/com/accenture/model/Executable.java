package com.accenture.model;

import com.accenture.threads.*;

public class Executable {
	public static void main(String[] args) throws Exception{
		Thread.currentThread().setPriority(8);
		System.out.println("The main thread is: "+ Thread.currentThread().getId()+ " having priority of "+Thread.currentThread().getPriority());
		Projection p = new Projection();
		Thread t1 = new Thread(p);
		
		Message t2 = new Message();
		t2.setPriority(7);
		t2.start();
		
		t1.join(2000);
		t2.join(3000);
	}
}
